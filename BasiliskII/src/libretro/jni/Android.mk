LOCAL_PATH := $(call my-dir)
CORE_DIR := $(LOCAL_PATH)/..

SOURCES_C    :=
SOURCES_ASM  :=
INCFLAGS     :=

COMMONFLAGS := -DHAVE_LOFF_T -D__LIBRETRO__

include $(CLEAR_VARS)
include $(CORE_DIR)/Makefile.common
LOCAL_MODULE       := retro
LOCAL_SRC_FILES    := $(SOURCES_C) $(SOURCES_CXX) $(SOURCES_ASM)
LOCAL_CFLAGS       := $(COMMONFLAGS) $(INCFLAGS)
LOCAL_CXXFLAGS     := $(COMMONFLAGS) $(INCFLAGS)
LOCAL_CPP_FEATURES := rtti exceptions
include $(BUILD_SHARED_LIBRARY)
